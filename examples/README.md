# Getting started

First of all, we need to import the module and instantiate a decoder object. We will use a decoder with HDC=2 and L1A=16:

```python
import pydecoder as pyd
decoder = pyd.fei4_decoder(HDC=2, L1A=16)
print("The decoder has HDC={hdc} and L1A={l1a}"\
      .format(hdc=decoder.HDC, l1a=decoder.L1A))
```

We can also read back those fields as shown in the last line. We now need to specify an input file and possibly the calibration:

```python
decoder.open_file("./B0_96_35ke_5ToT_60ke_Minibeam_SOURCE_SCAN_4_0_0_0.raw")

decoder.calA_from_ROOT_file(rootfile="./calibration_files/A.root", histoname="ParA_0_MA")
decoder.calB_from_ROOT_file(rootfile="./calibration_files/B.root", histoname="ParB_0_MA")
print("Calibration files set: A:{a} B:{b} C:{c}"\
      .format(a=decoder.calA_set, b=decoder.calB_set, c=decoder.calC_set))
decoder.calC_from_ROOT_file(rootfile="./calibration_files/C.root", histoname="ParC_0_MA")
print("Calibration files set: A:{a} B:{b} C:{c}"\
      .format(a=decoder.calA_set, b=decoder.calB_set, c=decoder.calC_set))
```
We can see how we can check if the calibration files have been set. The above code will print, given the files exist:

```
### Decoder INFO: opened file: ./B0_96_35ke_5ToT_60ke_Minibeam_SOURCE_SCAN_4_0_0_0.raw
### Decoder INFO: opened histogram: ./calibration_files/A.root:ParA_0_MA
### Decoder INFO: size check passed!
### Decoder INFO: opened histogram: ./calibration_files/B.root:ParB_0_MA
### Decoder INFO: size check passed!
Calibration files set: A:True B:True C:False
### Decoder INFO: opened histogram: ./calibration_files/C.root:ParC_0_MA
### Decoder INFO: size check passed!
Calibration files set: A:True B:True C:True
```

Any text starting with `### Decoder` is output from the C++ code, informing the user on the status. Now that we have the calibration files loaded, we can switch to ToT to charge decoding:

```python
decoder.use_abc_tot_decoder()
```

And we are ready to go!

# Processing events

To decode N events, just run:

```python
  trg_cnt, TD_cnt, events = decoder.decode_events(numberevents=N)
```

This will attempt to decode up to N events, if the end of the data file is reached before, this will return the actual number of read events as `trg_cnt`. If one wants to process blocks of 5000 events for example:

```python
events_to_process = 5000
trg_cnt = events_to_process
while trg_cnt == events_to_process:
  trg_cnt, TD_cnt, events = decoder.decode_events(numberevents=events_to_process)
  #do something with the events here
```

The variable `trg_cnt` must equal `len(events)`. `TD_cnt` gived the number of trigger words in the USBPix data stream. Depending on the trigger, these might be there or not, moreover they are known to glitch. [Confer the section on data integrity.](#Data-integrity)

If you want to process all events, just call `decode_events` (or with `numberevents=0`).

# Using the events

Now that we have a selection of events, let's look how to process them. The return type of `decode_events` is `Tuple[int, int, List[List[pydecoder.cluster]]]` where we already know that the first two `int`s are the number of events and trigger words. The last is a list of a list of clusters. The outer list loops over events, i.e. for each event there will be a list of clusters. If there were no clusters, this will be an empty list.

## Cluster information

If we just want to know the number of cluster per event, we just have to look at the `len()` of the cluster list.

```python
tr, TD, events = decoder.decode_events()

for event in events:
  clusters_per_event.append(len(event))

import matplotlib.pyplot as plt

x = range(0, tr)
plt.figure(figsize=(12, 6))
plt.scatter(x,clusters_per_event)
plt.title('clusters per event')
plt.xlabel('trigger [#]')
plt.ylabel('clusters [#]')
plt.savefig('./out/clusters_per_event.png')
```

![clusters per event image](out/clusters_per_event.png "Clusters Per Event")

## Hit information

We can also descend further, the following shows how to retrieve all the pixel information as well:

```python
cluster_charge = []
pixel_charge = []

for event in events:
  for cluster in event:
    cluster_charge.append(cluster.charge) 
    for hit in cluster.pixel_hits:
      pixel_charge.append(hit.charge)

# all the plotting
import matplotlib.pyplot as plt

plt.figure(figsize=(8, 8))
plt.hist(pixel_charge, range=(0,300000), bins=300, alpha=0.5, label="individual pixel charge")
plt.hist(cluster_charge, range=(0,300000), bins=300, alpha=0.5, label="cluster charge")
plt.legend()
plt.title('cluster charge distribution')
plt.xlabel('charge [e]')
plt.ylabel('counts')
plt.savefig('./out/charge.png')
```

![charge for hist and clusters](out/charge.png "Charge")

## GIF example

In the `example_gif.py` the ability to process only a fraction of the events is used. To create the full cluster per event map though, all the events are processed twice. Not really nice and could be improved. But the example has been added to show some of the possibilities. Output below.

![hitmap over time](histograms_gif/result.gif "GIF Example")


# Data integrity

## Corruption

In this example we decoded 30k events with L1A=16, which is the correct setting. We see that we get trigger words in the data stream. This allows us to check if the L1A was set correctly: in 30k triggers we get 29995 TDs which is a reasonable level of curruption.

```
### Decoder WARNING: spurious 'CHANNEL' detected in line 330103 after only 12 DHs
### Decoder WARNING: spurious data header (DH) detected in line 330105
	L1ID expected: 12 but got: 25
### Decoder WARNING: spurious trigger word (TD) detected in line 330107
	L1Accept expected: 16 but got: 1
### Decoder WARNING: spurious data header (DH) detected in line 669354
	L1ID expected: 7 but got: 20
### Decoder WARNING: spurious trigger word (TD) detected in line 669392
	L1Accept expected: 16 but got: 10
### Decoder WARNING: spurious 'CHANNEL' detected in line 1008607 after only 13 DHs
### Decoder WARNING: spurious data header (DH) detected in line 1008615
	L1ID expected: 31 but got: 12
### Decoder WARNING: spurious trigger word (TD) detected in line 1008663
	L1Accept expected: 16 but got: 14
### Decoder WARNING: spurious trigger word (TD) detected in line 1111661
	L1Accept expected: 16 but got: 11
### Decoder WARNING: spurious data header (DH) detected in line 1111673
	L1ID expected: 30 but got: 31
### Decoder WARNING: spurious trigger word (TD) detected in line 1111723
	L1Accept expected: 16 but got: 5
### Decoder WARNING: spurious 'CHANNEL' detected in line 1347862 after only 3 DHs
### Decoder WARNING: spurious data header (DH) detected in line 1347864
	L1ID expected: 23 but got: 4
### Decoder WARNING: spurious trigger word (TD) detected in line 1347924
	L1Accept expected: 16 but got: 14
### Decoder INFO: processed 30000 events
### Decoder INFO: processed 29995 trigger words!
```

To understand the corruption the USBPix readout system, below is the raw data file with added # to indicate different
events. 

```
...
0xe95baf
DH 0 22 943     # DH#14
0xe95bb0
DH 0 22 944     # DH#15
0x26a96f
DR 19 169 6 15
0x49258f
DR 36 293 8 15
0x5b1c8f
DR 45 284 8 15
0xe95bb1
DH 0 22 945      # DH#16
0x48bc8f
DR 36 188 8 15
############################
0xf84100
TD 2 1 5140       # TD
0xe95ec2
DH 0 23 706       # DH#1
0x3a816f
DR 29 129 6 15
0x42b1af
DR 33 177 10 15
0x72e85f
DR 57 232 5 15
0x653c8f
DR 50 316 8 15
0xe95ec3
DH 0 23 707       # DH#2
0xe95ec4
DH 0 23 708       # DH#3
###########################
CHANNEL 0         #spurious
0xe91209
DH 0 4 521        # DH#1
0xe9120a
DH 0 4 522        # DH#2
0x869c6f
DR 67 156 6 15
0x82ed7f
DR 65 237 7 15
0x60bd8f
DR 48 189 8 15
...
```

The second event starts correctly after the previous provided 16 DH, matching the set L1A. It starts with a trigger word: `TD 2 1 5140` but only has three DHs. Then a spurious `CHANNEL` is inserted in the data stream. This results in the warnings:

```bash
### Decoder WARNING: spurious 'CHANNEL' detected in line 1347862 after only 3 DHs
### Decoder WARNING: spurious data header (DH) detected in line 1347864
        L1ID expected: 23 but got: 4
```

## Wrong L1A

If the L1A is set wrongly, several things can go wrong. If instead of 16 for example 8,4,2, or 1 is set, not many additional warnings will show up. However, if there are TDs in the datastream, one will detect a mismatch. For example, if 8 is selected instead of 16 for the above example:

```bash
### Decoder INFO: processed 30000 events
### Decoder INFO: processed 14999 trigger words!
```

We see only half (minus a small bit of corruption) trigger words. There is no automatic warning in this case. Check your output carefully.

If instead of 16 one selects a L1A of 15, 14 or anything which does not fit integer wise into 16, there will be a lot of L1ID jumps and warnings.
