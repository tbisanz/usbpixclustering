import pydecoder as pyd
decoder = pyd.fei4_decoder(HDC=2, L1A=16)
print("The decoder has HDC={hdc} and L1A={l1a}"\
      .format(hdc=decoder.HDC, l1a=decoder.L1A))

decoder.open_file("./B0_96_35ke_5ToT_60ke_Minibeam_SOURCE_SCAN_4_0_0_0.raw")

decoder.calA_from_ROOT_file(rootfile="./calibration_files/A.root", histoname="ParA_0_MA")
decoder.calB_from_ROOT_file(rootfile="./calibration_files/B.root", histoname="ParB_0_MA")
print("Calibration files set: A:{a} B:{b} C:{c}"\
      .format(a=decoder.calA_set, b=decoder.calB_set, c=decoder.calC_set))
decoder.calC_from_ROOT_file(rootfile="./calibration_files/C.root", histoname="ParC_0_MA")
print("Calibration files set: A:{a} B:{b} C:{c}"\
      .format(a=decoder.calA_set, b=decoder.calB_set, c=decoder.calC_set))
decoder.use_abc_tot_decoder()

tr, TD, events = decoder.decode_events()

clusters_per_event = []
cluster_charge = []
pixel_charge = []

for event in events:
  clusters_per_event.append(len(event))
  for cluster in event:
    cluster_charge.append(cluster.charge) 
    for hit in cluster.pixel_hits:
      pixel_charge.append(hit.charge)

# all the plotting
import matplotlib.pyplot as plt

x = range(0, tr)
plt.figure(figsize=(12, 6))
plt.scatter(x,clusters_per_event)
plt.title('clusters per event')
plt.xlabel('trigger [#]')
plt.ylabel('clusters [#]')
plt.savefig('./out/clusters_per_event.png')

plt.figure(figsize=(8, 8))
plt.hist(pixel_charge, range=(0,300000), bins=300, alpha=0.5, label="individual pixel charge")
plt.hist(cluster_charge, range=(0,300000), bins=300, alpha=0.5, label="cluster charge")
plt.legend()
plt.title('cluster charge distribution')
plt.xlabel('charge [e]')
plt.ylabel('counts')
plt.savefig('./out/charge.png')
