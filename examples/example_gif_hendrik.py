import pydecoder
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

matplotlib.use('TKAgg')

decoder = pydecoder.fei4_decoder(0, 2);
decoder.set_spatial_cut_sqrd(9)

decoder.open_file("/ceph/groups/e4/users/hspeiser/public/Allpix_Validation_WPE/Messwerte_Output/Messung_26_08_23/raw_files_Messung_26_08_2023/50_DO37_04_10__5ke_10tot_17ke_SOURCE_SCAN_164_08_MeV_hm_100000_1_Scan_0_0_0.raw")

#discard first 8300 events, as they are empty
#decoder.decode_events()

x_size, y_size = 80, 336

all_events = []
axs = []
figs = []

rng = 250
a = rng
ix = 0
while ix < 20:
  hitmap = np.zeros(shape=(x_size, y_size), dtype=int, order='F')
  a, b, events = decoder.decode_events(rng)
  for event in events:
    for cluster in event:
      for hit in cluster.pixel_hits:
        hitmap[hit.x-1][hit.y-1] += 1
  fig, ax = plt.subplots(2,1,figsize=(10,14),gridspec_kw={'height_ratios': [6, 1]})
  ax[0].imshow(hitmap, interpolation='none', aspect=5, vmin=0, vmax=8)
  #plt.colorbar()
  ax[0].set_title('hitmap')
  ax[0].set_xlabel('x [px]')
  ax[0].set_ylabel('y [px]')
  ax[1].set_title('clusters per event')
  ax[1].set_xlabel('event [#]')
  ax[1].set_ylabel('cluster [#]')
  ix += 1
  all_events.extend(events)
  axs.append(ax)
  figs.append(fig)
  
clusters_per_event = []
for event in all_events:
  clusters_per_event.append(len(event))

out_path = "./histograms_gif/"

x_axis = range(0,len(all_events))
for i in range(0, len(figs)):
  axs[i][1].axvspan(i*rng, (i+1)*rng, color='red', alpha=0.5)
  axs[i][1].scatter(x_axis,clusters_per_event)
  number = str(i)
  number = number.rjust(3,'0')
  figs[i].savefig(out_path+'hitmap_'+number+'.png')

from PIL import Image
import glob

imgs = sorted(glob.glob(out_path+"hitmap_*.png"))
print(imgs)
frames = []
for i in imgs:
    new_frame = Image.open(i)
    frames.append(new_frame)

frames[0].save(out_path+'result.gif', format='GIF',
    append_images=frames[1:],
    save_all=True,
    duration=300, loop=0)

