#include <fstream>
#include <sstream>
#include <functional>
#include <limits>
#include <iostream>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>
namespace py = pybind11;

#include "TFile.h"
#include "TH2F.h"
#include "TCanvas.h"

namespace FEI4 {
  inline constexpr int NoHit = 0xF;
  inline constexpr int SmallHit = 0xE;
  inline constexpr size_t FE_ROWS = 336;
  inline constexpr size_t FE_COLS = 80;
};

namespace colour {
  inline constexpr std::string_view RED ("\033[0;31m");
  inline constexpr std::string_view YEL ("\033[0;33m");
  inline constexpr std::string_view BLU ("\033[0;34m");
  inline constexpr std::string_view RST ("\033[0m");
  inline constexpr std::string_view NOP ("");
};

/** A raw_hit is the (POD) object representing a data record (DR) in the
 *  FE-I4B datastream. It does not necessarily correspond to only a hit,
 *  as (depending on HitDiscConfig, HDC) a DR can also contain two hits for
 *  neighbouring pixels. The ToT values correspond to ToT code, not real
 *  ToT - i.e. they are not decoded according to HDC yet.
 *  This class is only used internally and not exposed in the Python bindings
 *  as it is only used in there intermediate file parsing */
struct raw_hit{
  int x;
  int y;
  int tot1;
  int tot2;
  int lvl1;
  raw_hit(int x, int y, int tot1, int tot2, int lvl1):
    x(x), y(y), tot1(tot1), tot2(tot2), lvl1(lvl1){}
  raw_hit() = delete;
};

/** A hit is the entity describing an actually hit pixel. The
 *  ToT it contains, corresponds to the real ToT (not ToT code).
 *  Moreover, the decoded charge is stored in units of the used
 *  decoder. This can be for example in electrons or true ToT if
 *  no decoder is used */
struct pixel_hit{
  int x;
  int y;
  int tot;
  bool is_small;
  double charge;
  int lvl1;
  pixel_hit(int x, int y, int tot, int lvl1, double charge, bool is_small = false): 
    x(x), y(y), tot(tot), is_small(is_small), charge(charge), lvl1(lvl1) {}
  pixel_hit() = delete;
};

struct cluster
{
  double x;
  double y;
  double charge;
  std::vector<pixel_hit> pixel_hits;
  cluster(double x, double y, double charge) : x(x), y(y), charge(charge) {}
  cluster(double x, double y, double charge, std::vector<pixel_hit> &&pixel_hits) : x(x), y(y), charge(charge) , pixel_hits(std::move(pixel_hits)) {}
  cluster() = delete;
};

template<typename T>
bool cal_from_ROOT_file(std::string const &file,
                        std::string const &name,
                        T& t, bool colour)
{
  auto f = TFile(file.c_str());
  if(!f.IsOpen()) {
    std::cout << (colour ? colour::RED : colour::NOP)
              << "### Decoder ERROR: problem opening file: " << file << '\n'
              << "\tCorrect path given? Make sure the file exists"
              << (colour ? colour::RST : colour::NOP) << '\n';
    return false;
  }
  auto h = static_cast<TH2F*>(f.Get(name.c_str()));
  if (h)
  {
    std::cout << (colour ? colour::BLU : colour::NOP)
              << "### Decoder INFO: opened histogram: " << file << ":" << name
              << (colour ? colour::RST : colour::NOP) << '\n';
    auto h_size_x = h->GetNbinsX();
    auto h_size_y = h->GetNbinsY();

    if ((FEI4::FE_COLS == h_size_x) &&
        (FEI4::FE_ROWS == h_size_y))
    {
      std::cout << (colour ? colour::BLU : colour::NOP)
              << "### Decoder INFO: size check passed!"
              << (colour ? colour::RST : colour::NOP) << '\n';
    }
    else
    {
      std::cout << (colour ? colour::RED : colour::NOP)
              << "### Decoder ERROR: size check FAILED!"
              << (colour ? colour::RST : colour::NOP) << '\n';
      return false;
    }

    for (size_t x = 0; x < FEI4::FE_COLS; ++x)
      for (size_t y = 0; y < FEI4::FE_ROWS; ++y)
        t.at(x * FEI4::FE_ROWS + y) = h->GetBinContent(x + 1, y + 1);
  }
  else
  {
    std::cout << (colour ? colour::RED : colour::NOP)
              << "### Decoder ERROR: problem opening histogram: " << name << '\n'
              << "Filedump: (" << file << ")\n";
    f.ls();
    std::cout << (colour ? colour::RST : colour::NOP);
    return false;
  }
  return true;
};

bool cal_from_nparray(py::array_t<double> from_arr, auto & to_arr, bool colour)
{
  auto view = from_arr.unchecked<2>();
  auto x_sz = view.shape(0);
  auto y_sz = view.shape(1);

  if(x_sz != FEI4::FE_COLS || y_sz != FEI4::FE_ROWS) {
    std::cout << (colour ? colour::RED : colour::NOP)
              << "### Decoder ERROR: size check FAILED!\n"
              << "Got size: " << x_sz << " x " << y_sz << " while expecting: " << FEI4::FE_COLS << " x " << FEI4::FE_ROWS << '\n'
              << (colour ? colour::RST : colour::NOP) << '\n';
    return false;
  }
  
  for (int x = 0; x < x_sz; ++x)
    for (int y = 0; y < y_sz; ++y) {
      to_arr.at(x * FEI4::FE_ROWS + y) = view(x, y);
    }
  return true;
};

using totdecoder_t = std::function<double(size_t x, size_t y, size_t tot)>;

/** Decoder for decoding raw_hit into hits with HitDiscConfig=0,
 *  The ToT Codes correspond to following values:
 *  15 (0xF): no hit
 *  0-13: ToT-1 (i.e. ToT Code 7 corresponds to a hit with real ToT 8)
 *  14 (0xE) is not a valid ToT */
std::vector<pixel_hit> decode_hits_HDC0( std::vector<raw_hit> const & rawHits, totdecoder_t const & totdecoder) {
  std::vector<pixel_hit> result;
  for(auto& raw: rawHits){
    auto charge = totdecoder(raw.x, raw.y, raw.tot1+1);
    result.emplace_back(raw.x, raw.y, raw.tot1+1, raw.lvl1, charge);
    //In case tot2 is not "no hit" we also need to add it
    if(raw.tot2 != FEI4::NoHit){
      auto charge2 = totdecoder(raw.x, raw.y+1, raw.tot2+1);
      result.emplace_back(raw.x, raw.y+1, raw.tot2+1, raw.lvl1, charge2);
    }
  }
  return result;
}

/** Decoder for decoding rawHits into hits with HitDiscConfig=1,
 *  The ToT Codes correspond to following values:
 *  15 (0xF): no hit
 *  0-13: ToT-2 (i.e. ToT Code 7 corresponds to a hit with real ToT 9)
 *  14 (0xE) is an small hit, i.e. real ToT 1 but only read 
 * out with an adjacent large hit */
std::vector<pixel_hit> decode_hits_HDC1( std::vector<raw_hit> const & rawHits, totdecoder_t const & totdecoder ) {
  std::vector<pixel_hit> result;
  for(auto const & raw: rawHits){
    int trueTot = (raw.tot1 == FEI4::SmallHit) ? 1 : raw.tot1+2;
    auto charge = totdecoder(raw.x, raw.y, trueTot);
    result.emplace_back(raw.x, raw.y, trueTot, raw.lvl1, charge, (raw.tot1 == FEI4::SmallHit));
    if(raw.tot2 != FEI4::NoHit){
      int trueTot2 = (raw.tot2 == FEI4::SmallHit) ? 1 : raw.tot2+2;
      auto charge2 = totdecoder(raw.x, raw.y+1, trueTot2);
      result.emplace_back(raw.x, raw.y+1, trueTot2, raw.lvl1, charge2, (raw.tot2 == FEI4::SmallHit));
    }
  }
  return result;
}

/** Decoder for decoding rawHits into hits with HitDiscConfig=2,
 *  The ToT Codes correspond to following values:
 *  15 (0xF): no hit
 *  0-13: ToT-3 (i.e. ToT Code 7 corresponds to a hit with real ToT 10)
 *  14 (0xE) is an small hit, i.e. real ToT 1 but only read 
 * out with an adjacent large hit */
std::vector<pixel_hit> decode_hits_HDC2( std::vector<raw_hit> const & rawHits, totdecoder_t const & totdecoder ) {
  std::vector<pixel_hit> result;
  for(auto const & raw: rawHits){
    int trueTot = (raw.tot2 == FEI4::SmallHit) ? 1 : raw.tot1+3;
    auto charge = totdecoder(raw.x, raw.y, trueTot);
    result.emplace_back(raw.x, raw.y, trueTot, raw.lvl1, charge, (raw.tot2 == FEI4::SmallHit));
    if(raw.tot2 != FEI4::NoHit){
      int trueTot2 = (raw.tot2 == FEI4::SmallHit) ? 1 : raw.tot2+3;
      auto charge2 = totdecoder(raw.x, raw.y+1, trueTot2);
      result.emplace_back(raw.x, raw.y+1, trueTot2, raw.lvl1, charge2, (raw.tot2 == FEI4::SmallHit));
    }
  }
  return result;
}

std::vector<cluster> clusterHits(std::vector<pixel_hit> const & hits, int spatCutSqrd, int tempCut, bool onlyCoG){
  //single pixel_hit events are easy, we just got one single hit cluster
  if(hits.size() == 1){
    std::vector<cluster> result;
    auto const & pix = hits[0];
    //For single pixel clusters the cluster position is in the centre of the pixel
    //Since we start counting at pixel 1 (not at 0), this is shifter by -0.5 px
    result.emplace_back(pix.x-0.5, pix.y-0.5, pix.charge);
    result.back().pixel_hits = hits;
    return result;
  //multi pixel_hit events are more complicated
  } else {
    std::vector<pixel_hit> hitPixelVec = hits;
    std::vector<pixel_hit> newlyAdded;    
    std::vector<cluster> clusters;

    while( !hitPixelVec.empty() ) {
      clusters.emplace_back(-1.,-1, 0);      
      newlyAdded.push_back( hitPixelVec.front() );
      clusters.back().pixel_hits.push_back( hitPixelVec.front() );
      hitPixelVec.erase( hitPixelVec.begin() );
      
      while( !newlyAdded.empty() ) {
        bool newlyDone = true;
        int  x1, x2, y1, y2, lv1, lv2, dX, dY, dLv;

        for( auto candidate = hitPixelVec.begin(); candidate != hitPixelVec.end(); ++candidate ){
          //get the relevant infos from the newly added pixel
          x1 = newlyAdded.front().x;
          y1 = newlyAdded.front().y;
          lv1 = newlyAdded.front().lvl1;

          //and the pixel we test against
          x2 = candidate->x;
          y2 = candidate->y;
          lv2 = candidate->lvl1;
          dX = x1-x2;
          dY = y1-y2;
          dLv = lv1-lv2;
          int spatDistSqrd = dX*dX+dY*dY;
          int tempDist = dLv*dLv;
  
          if( spatDistSqrd <= spatCutSqrd && tempDist <= tempCut) {
            newlyAdded.push_back( *candidate );  
            clusters.back().pixel_hits.push_back( *candidate );
            hitPixelVec.erase( candidate );
            newlyDone = false;
            break;
          }
        }
        if(newlyDone) {
          newlyAdded.erase(newlyAdded.begin());
        }
      }
    }

    for(auto& c: clusters) {
      float x_sum = 0;
      float y_sum = 0;
      float q_sum = 0.; 
      if( onlyCoG ) {
        for(auto const & h: c.pixel_hits) {
          x_sum += h.x;
          y_sum += h.y;
          q_sum += h.charge;
	      }
        c.x = x_sum/c.pixel_hits.size()-0.5;
        c.y = y_sum/c.pixel_hits.size()-0.5;
        c.charge = q_sum;
      } else {
        for(auto const & h: c.pixel_hits) {
          x_sum += h.x*h.charge;
          y_sum += h.y*h.charge;
          q_sum += h.charge;
        }
        c.x = x_sum/q_sum-0.5;
        c.y = y_sum/q_sum-0.5;
        c.charge = q_sum;
      }
    }
  return clusters;
  }      
}

class fei4_decoder
{
public:
  fei4_decoder(int HDC, int L1A): HDC(HDC), L1A(L1A) {};
  fei4_decoder() = delete;
  int HDC = 0;
  int L1A = 16;
  std::array<float, FEI4::FE_COLS * FEI4::FE_ROWS> calA;
  std::array<float, FEI4::FE_COLS * FEI4::FE_ROWS> calB;
  std::array<float, FEI4::FE_COLS * FEI4::FE_ROWS> calC;

  std::string previous_command;
  std::ifstream data_stream;
  size_t line_no = 0;
  bool colour = true;

  bool calA_set = false;
  bool calB_set = false;
  bool calC_set = false;

  size_t cut_dist_sqrd = 4;
  size_t cut_temp = 3;
  bool cog_only = false;

  const totdecoder_t one_decoder =
      [](size_t, size_t, size_t tot)
  {
    return tot;
  };

  const totdecoder_t abc_decoder =
      [&](size_t x, size_t y, size_t tot)
  {
    auto ix = (x-1) * FEI4::FE_ROWS + (y-1);
    return calA[ix]+calB[ix]*tot+calC[ix]*tot*tot;
  };

  totdecoder_t const * used_tot_decoder = &one_decoder;

  void use_abc_tot_decoder() {
    if(calA_set && calB_set && calC_set)
    {
      used_tot_decoder = &abc_decoder;
    }
    else
    {
      std::cout << (colour ? colour::RED : colour::NOP)
                << "### Decoder ERROR: attempted to set abc ToT decoder, but not all calibrations set!"
                << (colour ? colour::RST : colour::NOP) << '\n';
    }
  };
  
  void set_cog_clustering(){
    cog_only = true;
  }
  void set_q_weighed_clustering(){
    cog_only = false;
  }
  void set_spatial_cut_sqrd(int cut){
    cut_dist_sqrd = cut;
  }
  void set_temporal_cut(int cut){
    cut_temp = cut;
  }

  std::tuple<size_t, size_t, std::vector<std::vector<cluster>>> 
  decode_data(std::ifstream& infile, std::string& cmdDec, size_t& line_no, 
  totdecoder_t const & totdecoder, size_t no_events);

  void use_one_tot_decoder() {
    used_tot_decoder = &one_decoder;
  };

  int get_hdc() const { return HDC; };
  int get_l1a() const { return L1A; };

  void open_file(std::string const& filename) {
    data_stream = std::ifstream(filename);
    if(!data_stream.is_open())
    {
      std::cout << (colour ? colour::RED : colour::NOP)
                << "### Decoder ERROR: could not open file: " << filename
                << (colour ? colour::RST : colour::NOP) << '\n';
    } 
    else
    {
      std::cout << (colour ? colour::BLU : colour::NOP)
                << "### Decoder INFO: opened file: " << filename
                << (colour ? colour::RST : colour::NOP) << '\n';
    }
    line_no = 0;
  };

  std::tuple<size_t, size_t, std::vector<std::vector<cluster>>> 
  decode_events(size_t no_events = 0){
       return decode_data(data_stream, previous_command, line_no, 
                          *used_tot_decoder, no_events);
  };

  void calA_from_ROOT_file(std::string const &file, std::string const &name)
  {
    calA_set = cal_from_ROOT_file(file, name, calA, colour);
  };
  void calB_from_ROOT_file(std::string const &file, std::string const &name)
  {
    calB_set = cal_from_ROOT_file(file, name, calB, colour);
  };
  void calC_from_ROOT_file(std::string const &file, std::string const &name)
  {
    calC_set = cal_from_ROOT_file(file, name, calC, colour);
  };
  void calA_from_nparray(py::array_t<double, py::array::c_style | py::array::forcecast> array)
  {
    calA_set = cal_from_nparray(array, calA, colour);
  };
  void calB_from_nparray(py::array_t<double, py::array::c_style | py::array::forcecast> array)
  {
    calB_set = cal_from_nparray(array, calB, colour);
  };
  void calC_from_nparray(py::array_t<double, py::array::c_style | py::array::forcecast> array)
  {
    calC_set = cal_from_nparray(array, calC, colour);
  };

  void print_status()
  {
    std::cout << "CXX decoder - status\n\tHDC: " << HDC
              << "\n\tLV1A: " << L1A << std::endl;
  };

  void print_cal_maps()
  {
    auto calA_h = TH2F("calA", "calA;FEI4 column; FEI4 row", FEI4::FE_COLS, -0.5, FEI4::FE_COLS-0.5, FEI4::FE_ROWS, -0.5, FEI4::FE_ROWS-0.5);
    auto calB_h = TH2F("calB", "calB;FEI4 column; FEI4 row", FEI4::FE_COLS, -0.5, FEI4::FE_COLS-0.5, FEI4::FE_ROWS, -0.5, FEI4::FE_ROWS-0.5);
    auto calC_h = TH2F("calC", "calC;FEI4 column; FEI4 row", FEI4::FE_COLS, -0.5, FEI4::FE_COLS-0.5, FEI4::FE_ROWS, -0.5, FEI4::FE_ROWS-0.5);
    for(std::size_t x = 0; x < FEI4::FE_COLS; ++x)
     for(std::size_t y = 0; y < FEI4::FE_ROWS; ++y) {
      calA_h.Fill(x, y, calA.at(x * FEI4::FE_ROWS + y));
      calB_h.Fill(x, y, calB.at(x * FEI4::FE_ROWS + y));
      calC_h.Fill(x, y, calC.at(x * FEI4::FE_ROWS + y));
    }

    calA_h.SetStats(0);
    calB_h.SetStats(0);
    calC_h.SetStats(0);
    TCanvas c("c","c", 1200, 1200);
    calA_h.Draw("colz");
    c.SaveAs("CalA.svg");
    calB_h.Draw("colz");
    c.SaveAs("CalB.svg");
    calC_h.Draw("colz");
    c.SaveAs("CalC.svg");
  }
};

std::tuple<size_t, size_t, std::vector<std::vector<cluster>>> 
fei4_decoder::decode_data(std::ifstream& infile, std::string& cmdDec, size_t& line_no, 
totdecoder_t const & totdecoder, size_t no_events){

  if(no_events == 0) no_events = std::numeric_limits<size_t>::max();
  
  constexpr std::string_view DH = "DH";
  constexpr std::string_view DR = "DR";
  constexpr std::string_view TD = "TD";
  constexpr std::string_view CHANNEL = "CHANNEL";

  int _pHitDiscConf = HDC;
  size_t _pLv1ReadOut = L1A;

  std::vector<std::vector<cluster>> result;
  bool colour = true;

  //This is the trigger count for what we consider to be an event, this can be different
  //from the arriving trigger words (TŔ) - they might glitch. We will start a new event
  //if:
  // - we see a TR in the data stream
  // - a CHANNEL word is read in the data stream
  // - an uexpected  L1ID jump
  // - the number of expected DHs has been read out
  //Given the latter, if the expected LVL1 accept is wrongly set, the event building will
  //be wrong!
  size_t trigger = 0;
  //The DH counts the data headers for every trigger, resets every event
  size_t DH_count = 0;
  //The TR count for the entire processed datastream, integrates over all events
  size_t TR_count = 0;

  std::vector<raw_hit> hitVec;

  //internal tracker to keep track of the current L1ID, -1 indicates that there is no
  //expected L1ID, i.e. after reading out all expected L1accept this is set to -1
  int curr_l1id = -1;

  do {
    //Sometimes there is a "CHANNEL X" in the data file, this confuses our parser and
    //we need to correct for this, in this case the cmd is actually the second line we
    //read and the actual cmdDec is the third. The first line - i.e. "CHANNEL X" can
    //safely be discarded
    if( cmdDec.compare(0, CHANNEL.length(), CHANNEL) == 0 ) {
      if( DH_count != 0) {
          std::cout << (colour ? colour::YEL : colour::NOP)
                    << "### Decoder WARNING: spurious 'CHANNEL' detected in line "
                    << line_no << " after only " << DH_count << " DHs"
                    << (colour ? colour::RST : colour::NOP) << '\n';
        DH_count = _pLv1ReadOut;
      }
    }
    //The LvL1 is determined by counting the data header (DH), indicated by an entry
    //starting with "DH". Records containing data are so called data records, they start
    //with a "DR". In case an external trigger was provided, we also have trigger words
    //("TD") in the datastream. They are counted for statistics and verification
    if( cmdDec.compare(0, DH.length(), DH) == 0 ) {
      DH_count++;
      //If we're one DH above what we expect, we expect the L1ID to increase
      //but roll over at 32; the reset of the DH count will be done by the clustering
      if(DH_count == _pLv1ReadOut+1) curr_l1id =(curr_l1id+1)%32;
      int a,b,c;
      std::string word;
      std::stringstream line(cmdDec);
      if(line >> word >> a >> b >> c) {
        //std::cout << "Before L1ID: " << curr_l1id << '\n';
        if(curr_l1id == -1) {
          curr_l1id = b;
        } else if (curr_l1id != b) {
          std::cout << (colour ? colour::YEL : colour::NOP)
                    << "### Decoder WARNING: spurious data header (DH) detected in line "
                    << line_no << "\n\tL1ID expected: " << curr_l1id << " but got: " << b
                    << (colour ? colour::RST : colour::NOP) << '\n';
          curr_l1id = b;
          //This will trigger clustering
          DH_count = _pLv1ReadOut+1;
        }
        //std::cout << "DH - count (" << DH_count << ") L1ID: " << curr_l1id << '\n';
      }
    } else if( cmdDec.compare(0, DR.length(), DR) == 0 ) {
      int x, y, tot1, tot2;
      std::string word;
      std::stringstream line(cmdDec);
      if(line >> word >> x >> y >> tot1 >> tot2) {
        hitVec.emplace_back(x, y, tot1, tot2, DH_count-1);
        //std::cout << "Added event to " << curr_l1id << '\n';
      } else {
        //PANIC!
      }
    } else if( cmdDec.compare(0, TD.length(), TD) == 0 ) {
        //just for monitoring
        TR_count++;
        //curr_l1id = -1;
        //if(DH_count != 0) {
        //  std::cout << (colour ? colour::YEL : colour::NOP)
        //            << "### Decoder WARNING: spurious trigger word (TD) detected in line "
        //            << line_no << "\n\tL1Accept expected: " <<  _pLv1ReadOut << " but got: "
        //            << DH_count
        //            << (colour ? colour::RST : colour::NOP) << '\n';
        //DH_count = _pLv1ReadOut;
        //}
    }
    //We always read out a fixed number of data headers - if we reach this number, we will
    //start clustering and increment the (internal) trigger counter 
    if(DH_count == _pLv1ReadOut+1) {
      std::cout << "...clustering!\n";
      //Since we've already read out the next DH, this is set to 1
      DH_count = 1;
      trigger++;
      std::vector<pixel_hit> decodedHits;
      switch(_pHitDiscConf) {
        case 0:
          decodedHits = decode_hits_HDC0(hitVec, totdecoder);
          break;
        case 1:
          decodedHits = decode_hits_HDC1(hitVec, totdecoder);
          break;
        case 2:
          decodedHits = decode_hits_HDC2(hitVec, totdecoder);
          break;      
      }
      //here we cluster
      result.emplace_back(clusterHits(decodedHits, cut_dist_sqrd, cut_temp, cog_only));
      hitVec.clear();
    }
  } while ((trigger < no_events) && std::getline(infile, cmdDec) && ++line_no);

  //if(cmdDec.compare(0, DH.length(), DH) == 0) cmdDec.clear();
  std::cout << (colour ? colour::BLU : colour::NOP)
            << "### Decoder INFO: processed " << trigger << " events\n"
            << "### Decoder INFO: processed " << TR_count << " trigger words!"
            << (colour ? colour::RST : colour::NOP) << '\n';
  return {trigger, TR_count, result};
}

PYBIND11_MODULE(pydecoder, m)
{
  m.doc() = "pybind11 binding to the C++ FEI4 data decoding library";
  py::class_<pixel_hit>(m, "pixel_hit", "class representing a decoded pixel hit")
      .def(py::init<int, int, int, int, double, bool>(), "constructor",
           py::arg("x"), py::arg("y"), py::arg("tot"), py::arg("lvl1"), 
           py::arg("charge"), py::arg("is_small"))
      .def_readwrite("x", &pixel_hit::x, "x position of the hit in units of pixel index")
      .def_readwrite("y", &pixel_hit::y, "y position of the hit in units of pixel index")
      .def_readwrite("tot", &pixel_hit::tot, "decoded ('real') time over threshold")
      .def_readwrite("lvl1", &pixel_hit::lvl1, "lvl1 of the hit")
      .def_readwrite("charge", &pixel_hit::charge, "decoded charge, unit depends on the decoder")
      .def_readwrite("is_small", &pixel_hit::is_small, "flag indicating if the pixel was a small hit");
  py::class_<cluster>(m, "cluster", "class representing a cluster of pixels")
      .def(py::init<double, double, double>(), "constructor",
           py::arg("x"), py::arg("y"), py::arg("charge"))
      .def(py::init<double, double, double, std::vector<pixel_hit>>(), "constructor",
           py::arg("x"), py::arg("y"), py::arg("charge"), py::arg("pixel_hits"))
      .def_readwrite("x", &cluster::x)
      .def_readwrite("y", &cluster::y)
      .def_readwrite("charge", &cluster::charge)
      .def_readwrite("pixel_hits", &cluster::pixel_hits);
  py::class_<fei4_decoder>(m, "fei4_decoder")
      .def(py::init<int, int>(), "constructor",
           py::arg("HDC"), py::arg("L1A"))
      .def_readonly("HDC", &fei4_decoder::HDC)
      .def_readonly("L1A", &fei4_decoder::L1A)
      .def_readonly("calA_set", &fei4_decoder::calA_set)
      .def_readonly("calB_set", &fei4_decoder::calB_set)
      .def_readonly("calC_set", &fei4_decoder::calC_set)
      .def("use_abc_tot_decoder", &fei4_decoder::use_abc_tot_decoder)   
      .def("use_one_tot_decoder", &fei4_decoder::use_one_tot_decoder)  
      .def("open_file", &fei4_decoder::open_file, py::arg("filename"))
      .def("decode_events", &fei4_decoder::decode_events, py::arg("numberevents") = 0)
      .def("calA_from_ROOT_file", &fei4_decoder::calA_from_ROOT_file, 
           "set the calA coefficients from ROOT file", py::arg("rootfile"), py::arg("histoname"))
      .def("calB_from_ROOT_file", &fei4_decoder::calB_from_ROOT_file, 
           "set the calB coefficients from ROOT file", py::arg("rootfile"), py::arg("histoname"))
      .def("calC_from_ROOT_file", &fei4_decoder::calC_from_ROOT_file, 
           "set the calC coefficients from ROOT file", py::arg("rootfile"), py::arg("histoname"))
      .def("calA_from_nparray", &fei4_decoder::calA_from_nparray, 
           "set the calA coefficients from numpy array", py::arg("nparray"))
      .def("calB_from_nparray", &fei4_decoder::calB_from_nparray, 
           "set the calB coefficients from numpy array", py::arg("nparray"))
      .def("calC_from_nparray", &fei4_decoder::calC_from_nparray, 
           "set the calC coefficients from numpy array", py::arg("nparray"))
      .def("print_status", &fei4_decoder::print_status)
      .def("print_cal_maps", &fei4_decoder::print_cal_maps)
      .def("set_cog_clustering", &fei4_decoder::set_cog_clustering)
      .def("set_q_weighed_clustering", &fei4_decoder::set_q_weighed_clustering)
      .def("set_temporal_cut", &fei4_decoder::set_temporal_cut, "set the time cut in units of BC", py::arg("BC_cut"))
      .def("set_spatial_cut_sqrd", &fei4_decoder::set_spatial_cut_sqrd, "set the distance cut in pixels squared", py::arg("px_cut_sqrd"));
}
