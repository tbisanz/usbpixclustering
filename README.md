# Prerequisites

This is a C++ library with python bindings to decode USBPix raw data files. To build the library, a C++20 compliant compiler (e.g. gcc) is needed, in addition to the pybind11 library and ROOT. ROOT is used for loading ToT calibration files, which are provided by the STControl software.

## LCG release

The easiest and best maintained way to get all the requirements is to use the LCG release on CVMFS. If CERN's SFT CVMFS is mounted on your machines, just do:

`source /cvmfs/sft.cern.ch/lcg/views/LCG_102/x86_64-centos7-gcc11-opt/setup.sh`

Which will retrieve all the required packages.

```bash
[tbisanz@avior ~]$ which root
/cvmfs/sft.cern.ch/lcg/views/LCG_102/x86_64-centos7-gcc11-opt/bin/root
[tbisanz@avior ~]$ which c++
/cvmfs/sft.cern.ch/lcg/releases/gcc/11.2.0-8a51a/x86_64-centos7/bin/c++
[tbisanz@avior ~]$ which python3
/cvmfs/sft.cern.ch/lcg/views/LCG_102/x86_64-centos7-gcc11-opt/bin/python3
```

# Building the library

All of the library is contained within a single file, `cxx_clustering.cpp`. In order to build a shared library out of it, simply run:

`c++ -O3 -std=c++20 -Wextra -Wpedantic -Wall -Werror -shared  -fPIC $(python3 -m pybind11 --includes) cxx_clustering.cpp $(root-config --libs) -o pydecoder$(python3-config --extension-suffix)`

This tells the `c++` compiler (gcc) to perform an optimsed (`-O3`) build with the C++20 ISO standard (`-std=c++20`). Several warnings are enabled (`-Wextra -Wpedantic -Wall`) and are treated as errors (`-Werror`). We're building a shared library (`-shared`) which requires generation of position independent code (`-fPIC`). We ask Python to tell us its include files, including the ones for the pybind11 module (`python3 -m pybind11 --includes`). The only file we include is the `cxx_clustering.cpp`. We need to specify all the ROOT libraries which need to be linked, this is done via `root-config --libs`. The output (`-o`) of this step will be the file `pydecoder` with the sufffix provided by `python3-config --extension-suffix`.

## ROOT includes

In this example, we are using the ROOT provided by the LCG release. Sourcing the LCG release will set the `$C_INCLUDE_PATH` in your environment. These already ships many includes, also ROOT. If you use your own ROOT installation, you might need to specify the ROOT includes:

`-isystem$(root-config --incdir)`

Put this after `-fPIC`, it will tell your compiler to search for the ROOT includes in the path provided by `root-config`. Including them via `-isystem` will prevent any warnings in ROOT to cause problems, opposed to using `-I`.

`c++ -O3 -std=c++20 -Wextra -Wpedantic -Wall -Werror -shared  -fPIC -isystem$(root-config --incdir) $(python3 -m pybind11 --includes) cxx_clustering.cpp $(root-config --libs) -o pydecoder$(python3-config --extension-suffix)`

This will generate something like `pydecoder.cpython-39-x86_64-linux-gnu.so` (slight variations with different versions to be expected). Easiest way is to copy this library to your python files. Some more smart way might also exist.

# Using the library

The library exports three classes:

## Class: pixel_hit

The class `pixel_hit` is a POD (*plain-old-data*) structure. I.e., it just holds trivial member variables which can be read and modified.

 * `pixel_hit.x` (int) - the x position in pixel indices of a FEI4 hit (starts counting at 1 and goes to 80)
 * `pixel_hit.y` (int) - the y position in pixel indices of a FEI4 hit (starts counting at 1 and goes to 336)
 * `pixel_hit.tot` (int) - the real time-over threshold
 * `pixel_hit.lvl1` (int) - the lvl1 accept bin the hit was registered in
 * `pixel_hit.charge` (float) - depending on the tot decoder, this is either ToT or charge (in electrons)
 * `pixel_hit.is_small` (bool) - if this is a small hit; small hits always get a tot of 1 assigned

All members have the property `.def_readwrite` which makes them read and write able.

The constructor is as follows:
```python
    1. pydecoder.pixel_hit(x: int, y: int, tot: int, lvl1: int, charge: float, is_small: bool)
```


## Class: cluster

Also the `cluster` is a POD object. It holds:
 * `cluster.x` (float) - the x position of the cluster in pixel indices
 * `cluster.y` (float) - the y position of the cluster in pixel indices
 * `cluster.charge` (float) - sum of all the `pixel_hit.charge` in the cluster with their unit (ToT or charge)
 * `cluster.pixel_hits` (List of `pixel_hit`) - the list of hits attached to the cluster

All members have the property `.def_readwrite` which makes them read and write able.

The constructors are as follows:
```python
    1. pydecoder.cluster(x: float, y: float, charge: float)
    2. pydecoder.cluster(x: float, y: float, charge: float, pixel_hits: List[pydecoder.pixel_hit])
```

There is no internal implementation of any clustering algorithm in this class, i.e. you can instantiate cluster objects with no hits attached or wrongly computed positions. In nearly all cases, no need to instantiate custom clusters will be needed. They will be provided by the framework.

## Class: fei4_decoder

The `fei4_decoder` is a class with internal state. It can store ToT calibrations and process raw data files. In this process, it will provide clusters per event. It is constructed via:

```python
    1. pydecoder.fei4_decoder(HDC: int, L1A: int)
```

Where `HDC` is the FEI4 HitDiscConfig used to aquire the raw file, and `L1A` the amount of lvl1 accept, i.e. the amount of 25 ns bins read out (for the FEI4 this can be up to 16). These two members are exposed `.def_readonly` via:

 * `fei4_decoder.HDC` (int) - the selected HDC
 * `fei4_decoder.L1A` (int) - the selected L1A

 ### Cuts

The cuts used to determine if a pixel is considered a cluster. The checks are `value <= cut`. For the spatial cut the squared distance is used. I.e. pixels sharing an edge have a distance squared of (1+0)², pixels touching at one corner have a distance squared of (1+1)²=4. Default cuts are 4 for the distance squared and 3 for the temporal cut.

 * `fei4_decoder.set_temporal_cut(BC_cut: int)`
 * `fei4_decoder.set_spatial_cut_sqrd(px_cut_sqrd: int)`

 ### Clustering algorihtms

By default, no charge information is used to determine teh cluster centre. To enable this, call `set_q_weighed_clustering`.

 * `fei4_decoder.set_cog_clustering()`
 * `fei4_decoder.set_q_weighed_clustering()`

 ### ToT calibration

While ideally, the charge measurement of the FEI4 (ToT) should scale linear with the detected charge in electrons, there is a slight non-linearity which is approximated by a second-order-polynomial fit. This is done for each pixel, the charge is computed:

```math
q_i  = a_i+b_i\times ToT+c_i\times ToT^2
```

For this, we need the $`a_i, b_i, c_i`$ which are exported by STControl in hisograms stored in (a) ROOT file(s). For this, call:

```python
     my_decoder.calA_from_ROOT_file(self: pydecoder.fei4_decoder, rootfile: str, histoname: str) -> None
```

With the given ROOT file, where at top level the specified histogram can be retrieved. This function also exists for the other two coefficients: `calB_from_ROOT_file` and `calC_from_ROOT_file` with the same signature.

If the coefficients succesfully load, the `.def_readonly` exposed fields will indicate this:

 * `fei4_decoder.calA_set` (bool) - indicates if calibration coefficients for $`a_i`$ have been set
 * `fei4_decoder.calB_set` (bool) - indicates if calibration coefficients for $`b_i`$ have been set
 * `fei4_decoder.calC_set` (bool) - indicates if calibration coefficients for $`c_i`$ have been set

Once all three are set, one can enable the ToT to change conversion via:

```python
  my_decoder.use_abc_tot_decoder()
```

The default charge conversion which justs sets the ToT as charge can be enabled via:

```python
  my_decoder.use_one_tot_decoder()
```

